package ru.tsk.ilina.tm.controller;

import ru.tsk.ilina.tm.api.controller.ITaskController;
import ru.tsk.ilina.tm.api.service.ITaskService;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        for (Task task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

    @Override
    public void removeByID() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findByID(id);
        if (task == null) {
            System.out.println("[INCORRECT VALUES]");
            return;
        }
        taskService.removeByID(id);
    }

    @Override
    public void removeByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[INCORRECT INDEX]");
            return;
        }
        taskService.removeByIndex(index);
    }

    @Override
    public void removeByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findByName(name);
        if (task == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        taskService.removeByName(name);
    }

    public void showTask(Task task) {
        System.out.println("[SHOW PROJECT]");
        System.out.println(task);
        System.out.println("[OK]");
    }

    @Override
    public void findByID() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findByID(id);
        if (task == null) {
            System.out.println("[INCORRECT VALUES]");
            return;
        }
        showTask(task);
    }

    @Override
    public void findByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[INCORRECT INDEX]");
            return;
        }
        showTask(task);
    }

    @Override
    public void findByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findByName(name);
        if (task == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        showTask(task);
    }

    @Override
    public void updateByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[INCORRECT INDEX]");
            return;
        }
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Task updateTask = taskService.updateByIndex(index, name, description);
        if (updateTask == null) System.out.println("[INCORRECT VALUE]");
    }

    @Override
    public void updateById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findByID(id);
        if (task == null) {
            System.out.println("[INCORRECT ID]");
            return;
        }
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Task updateTask = taskService.updateById(id, name, description);
        if (updateTask == null) System.out.println("[INCORRECT VALUE]");
    }

}
