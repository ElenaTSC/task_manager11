package ru.tsk.ilina.tm.controller;

import ru.tsk.ilina.tm.api.controller.IProjectController;
import ru.tsk.ilina.tm.api.service.IProjectService;
import ru.tsk.ilina.tm.constant.TerminalConst;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = projectService.findAll();
        for (Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void removeByID() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findByID(id);
        if (project == null) {
            System.out.println("[INCORRECT VALUES]");
            return;
        }
        projectService.removeByID(id);
    }

    @Override
    public void removeByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[INCORRECT INDEX]");
            return;
        }
        projectService.removeByIndex(index);
    }

    @Override
    public void removeByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        projectService.removeByName(name);
    }

    public void showProject(Project project) {
        System.out.println("[SHOW PROJECT]");
        System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void findByID() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findByID(id);
        if (project == null) {
            System.out.println("[INCORRECT VALUES]");
            return;
        }
        showProject(project);
    }

    @Override
    public void findByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[INCORRECT INDEX]");
            return;
        }
        showProject(project);
    }

    @Override
    public void findByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        showProject(project);
    }

    @Override
    public void updateByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[INCORRECT INDEX]");
            return;
        }
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Project updateProject = projectService.updateByIndex(index, name, description);
        if (updateProject == null) System.out.println("[INCORRECT VALUE]");
    }

    @Override
    public void updateById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findByID(id);
        if (project == null) {
            System.out.println("[INCORRECT ID]");
            return;
        }
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Project updateProject = projectService.updateById(id, name, description);
        if (updateProject == null) System.out.println("[INCORRECT VALUE]");
    }

}
