package ru.tsk.ilina.tm;

import ru.tsk.ilina.tm.component.Bootstrap;


public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}