package ru.tsk.ilina.tm.api.repository;

import ru.tsk.ilina.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
