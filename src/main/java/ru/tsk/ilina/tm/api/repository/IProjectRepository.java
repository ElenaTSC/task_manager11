package ru.tsk.ilina.tm.api.repository;

import ru.tsk.ilina.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project removeByID(String id);

    Project removeByIndex(Integer index);

    Project removeByName(String name);

    Project findByID(String id);

    Project findByIndex(Integer index);

    Project findByName(String name);

    void add(Project project);

    void remove(Project project);

    void clear();

    List<Project> findAll();

}
