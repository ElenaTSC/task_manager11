package ru.tsk.ilina.tm.api.controller;

import ru.tsk.ilina.tm.model.Project;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

    void removeByID();

    void removeByIndex();

    void removeByName();

    void findByID();

    void findByIndex();

    void findByName();

    void updateById();

    void updateByIndex();

}
