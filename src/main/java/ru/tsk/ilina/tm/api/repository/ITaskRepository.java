package ru.tsk.ilina.tm.api.repository;


import ru.tsk.ilina.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    void clear();

    List<Task> findAll();

    Task removeByID(String id);

    Task removeByIndex(Integer index);

    Task removeByName(String name);

    Task findByID(String id);

    Task findByIndex(Integer index);

    Task findByName(String name);

}
