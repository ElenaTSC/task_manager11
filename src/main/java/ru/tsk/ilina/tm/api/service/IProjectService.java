package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project removeByID(String id);

    Project removeByIndex(Integer index);

    Project removeByName(String name);

    Project findByID(String id);

    Project findByIndex(Integer index);

    Project findByName(String name);

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    void clear();

    List<Project> findAll();

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
