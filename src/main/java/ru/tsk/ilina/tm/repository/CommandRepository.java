package ru.tsk.ilina.tm.repository;

import ru.tsk.ilina.tm.api.repository.ICommandRepository;
import ru.tsk.ilina.tm.constant.ArgumentConst;
import ru.tsk.ilina.tm.constant.TerminalConst;
import ru.tsk.ilina.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            " Display system info."
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            " Display developer info."
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            " Display program version."
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            " Display list of commands."
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            " Display list arguments."
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            " Display list commands."
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            " Close application."
    );

    public static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            " Create Task"
    );

    public static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            " Remove Tasks"
    );

    public static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            " Display list Task"
    );

    public static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            " Create Project"
    );

    public static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            " Remove Project"
    );

    public static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            " Display list Project"
    );

    public static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null,
            " Show Project by ID"
    );

    public static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null,
            " Show Project by Index"
    );

    public static final Command PROJECT_SHOW_BY_NAME = new Command(
            TerminalConst.PROJECT_SHOW_BY_NAME, null,
            " Show Project by Name"
    );

    public static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null,
            " Remove Project by Id"
    );

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null,
            " Remove Project by Index"
    );

    public static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME, null,
            " Remove Project by Name"
    );

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null,
            " Update Project by Index"
    );

    public static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null,
            " Update Project by Id"
    );

    public static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null,
            " Show Task by ID"
    );

    public static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null,
            " Show Task by Index"
    );

    public static final Command TASK_SHOW_BY_NAME = new Command(
            TerminalConst.TASK_SHOW_BY_NAME, null,
            " Show Task by Name"
    );

    public static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null,
            " Remove Task by Id"
    );

    public static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null,
            " Remove Task by Index"
    );

    public static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME, null,
            " Remove Task by Name"
    );

    public static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null,
            " Update Task by Index"
    );

    public static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null,
            " Update Task by Id"
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, ABOUT, HELP, VERSION, ARGUMENTS, COMMANDS, TASK_CREATE,
            TASK_CLEAR, TASK_LIST, TASK_SHOW_BY_ID, TASK_SHOW_BY_NAME, TASK_SHOW_BY_INDEX,
            TASK_REMOVE_BY_NAME, TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX, PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_NAME, PROJECT_SHOW_BY_INDEX,
            PROJECT_REMOVE_BY_NAME, PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX, EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
