package ru.tsk.ilina.tm.repository;

import ru.tsk.ilina.tm.api.repository.IProjectRepository;
import ru.tsk.ilina.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public Project removeByID(final String id) {
        final Project project = findByID(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project findByID(final String id) {
        for (Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project findByName(final String name) {
        for (Project project : projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public void clear() {
        projects.clear();

    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

}
